kf_web_standard_project

This is the reference implementation for all my JSF samples. If this works then you should be able to run all the others.

You must first install web_project_dependencies.

The glassfish-resources.xml has been renamed payara-resources.xml

To use rename this project and update the artifactId and groupId in the pom.xml 
Add your i18n bundles to faces-config.xml

You will also have to make changes to payara-resources.xml, persistence.xml and arquillian.xml
